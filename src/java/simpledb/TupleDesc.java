package simpledb;

import java.io.Serializable;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc implements Serializable {

    /**
     * A help class to facilitate organizing the information of each field
     * */
    public ArrayList<TDItem> TD=new ArrayList<>();;
    public static class TDItem implements Serializable {

        private static final long serialVersionUID = 1L;
        /**
         * The type of the field
         * */
        public final Type fieldType;

        /**
         * The name of the field
         * */
        public final String fieldName;

        public TDItem(Type t, String n) {
            this.fieldName = n;
            this.fieldType = t;
        }

        public String toString() {
            return fieldName + "(" + fieldType + ")";
        }
    }

    /**
     * @return
     *        An iterator which iterates over all the field TDItems
     *        that are included in this TupleDesc
     * */
    public Iterator<TDItem> iterator() {
        // some code goes here
        //return null;
        return this.TD.iterator();
    }

    private static final long serialVersionUID = 1L;

    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     *
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     * @param fieldAr
     *            array specifying the names of the fields. Note that names may
     *            be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
        for(int i=0;i<typeAr.length;i++)
        {
            TDItem temp=new TDItem(typeAr[i],fieldAr[i]);
            this.TD.add(temp);
        }
        // some code goes here
    }

    /**
     * Constructor. Create a new tuple desc with typeAr.length fields with
     * fields of the specified types, with anonymous (unnamed) fields.
     *
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     */
    public TupleDesc(Type[] typeAr) {
        // some code goes here
        for (Type type : typeAr) {
            TDItem temp = new TDItem(type, null);
            this.TD.add(temp);
        }
    }

    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
        // some code goes here
        return this.TD.size();
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     *
     * @param i
     *            index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {
        // some code goes here
        if(i>=this.numFields()||i<0) throw new NoSuchElementException("定位不到该元素");
        return this.TD.get(i).fieldName;
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     *
     * @param i
     *            The index of the field to get the type of. It must be a valid
     *            index.
     * @return the type of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public Type getFieldType(int i) throws NoSuchElementException {
        // some code goes here
        return this.TD.get(i).fieldType;
    }

    /**
     * Find the index of the field with a given name.
     *
     * @param name
     *            name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException
     *             if no field with a matching name is found.
     */
    public int fieldNameToIndex(String name) throws NoSuchElementException {
        // some code goes here
        for (int i = 0; i < TD.size(); i++) {
            TDItem tdItem = TD.get(i);
            if (Objects.equals(tdItem.fieldName, name)) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     *         Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
        //if(this.TD==null) return 0;
        int num=0;
        for(int i=0;i<this.TD.size();i++)
        {
           num+=this.TD.get(i).fieldType.getLen();
        }
        // some code goes here
        return num;
    }

    /**
     * Merge two TupleDescs into one, with td1.numFields + td2.numFields fields,
     * with the first td1.numFields coming from td1 and the remaining from td2.
     *
     * @param td1
     *            The TupleDesc with the first fields of the new TupleDesc
     * @param td2
     *            The TupleDesc with the last fields of the TupleDesc
     * @return the new TupleDesc
     */
    public static TupleDesc merge(TupleDesc td1, TupleDesc td2) {
        // some code goes here

        Type[] typeAr=new Type[td1.numFields()+td2.numFields()];
        String[] fieldAr=new String[td1.numFields()+td2.numFields()];

        for(int i=0;i<=td1.numFields()+td2.numFields()-1;i++)
        {
            if(i<td1.numFields()) {
                typeAr[i] = td1.TD.get(i).fieldType;
                fieldAr[i]=td1.TD.get(i).fieldName;
            }
            else{
                typeAr[i] = td2.TD.get(i-td1.numFields()).fieldType;
                fieldAr[i]=td2.TD.get(i-td1.numFields()).fieldName;
            }
        }
        return new TupleDesc(typeAr,fieldAr);
    }

    /**
     * Compares the specified object with this TupleDesc for equality. Two
     * TupleDescs are considered equal if they have the same number of items
     * and if the i-th type in this TupleDesc is equal to the i-th type in o
     * for every i.
     *
     * @param o
     *            the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */

    public boolean equals(Object o) {
        // some code goes here
        // return false;
        if(o==null)
            return false;
        if(o==this)
            return true;
        if (getClass() != o.getClass())
            return false;
        TupleDesc ret=(TupleDesc) o;
        if(((TupleDesc) o).numFields()!=this.numFields())
            return false;
        for (int i = 0; i < this.numFields(); i++) {
            if (!Objects.equals(this.TD.get(i).fieldName, ret.TD.get(i).fieldName)) {
                return false;
            }
            if (!Objects.equals(this.TD.get(i).fieldType, ret.TD.get(i).fieldType)) {
                return false;
            }

        }
        return true;
    }

    public int hashCode() {
        // If you want to use TupleDesc as keys for HashMap, implement this so
        // that equal objects have equals hashCode() results
        throw new UnsupportedOperationException("unimplemented");
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     *
     * @return String describing this descriptor.
     */
    public String toString() {
        String temp="";

        for(int i=0;i<this.numFields()-1;i++)
        {
            temp+=this.TD.get(0).toString()+',';
        }
        temp+=this.TD.get(this.numFields()-1).toString();
        // some code goes here
        return temp;
    }
}


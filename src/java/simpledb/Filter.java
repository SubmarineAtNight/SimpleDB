package simpledb;

import java.io.IOException;
import java.util.*;

/**
 * Filter is an operator that implements a relational select.
 */
public class Filter extends Operator {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor accepts a predicate to apply and a child operator to read
     * tuples to filter from.
     * 
     * @param p
     *            The predicate to filter tuples with
     * @param child
     *            The child operator
     */
    private Predicate myPredicate;
    private OpIterator mychild;
    public Filter(Predicate p, OpIterator child) {
        // some code goes here
        this.myPredicate=p;
        this.mychild=child;
    }

    public Predicate getPredicate() {
        // some code goes here
        return this.myPredicate;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        return this.mychild.getTupleDesc();
       // this.mychild.
    }

    public void open() throws DbException, NoSuchElementException,
            TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
        this.mychild.open();
        super.open();
    }

    public void close() throws TransactionAbortedException, IOException, DbException, InterruptedException {
        // some code goes here
        this.mychild.close();
        super.close();
    }

    public void rewind() throws DbException, TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
          this.mychild.rewind();
}

    /**
     * AbstractDbIterator.readNext implementation. Iterates over tuples from the
     * child operator, applying the predicate to them and returning those that
     * pass the predicate (i.e. for which the Predicate.filter() returns true.)
     * 
     * @return The next tuple that passes the filter, or null if there are no
     *         more tuples
     * @see Predicate#filter
     */
    protected Tuple fetchNext() throws NoSuchElementException,
            TransactionAbortedException, DbException, IOException, InterruptedException {
        // some code goes here
        while(this.mychild.hasNext())
        {
            Tuple t = mychild.next();
            if(this.myPredicate.filter(t))
                return t;
        }
        return null;
    }

    @Override
    public OpIterator[] getChildren() {
        // some code goes here
        return new OpIterator[] { this.mychild };
    }

    @Override
    public void setChildren(OpIterator[] children) {
        // some code goes here
        this.mychild=children[0];
    }

}

package simpledb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static simpledb.Aggregator.Op.COUNT;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    private Map<Field,Integer> aggregator_result=new HashMap<>();
    private TupleDesc my_TupleDesc;
    private int resultWhenNoGrouping;

    private int my_gbfield;
    private Type my_gbfieldtype;
    private int my_afield;
    private Op my_what;


    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
        if(what!=COUNT)
            throw new IllegalArgumentException("不是COUNT");

        this.my_gbfield=gbfield;
        this.my_gbfieldtype=gbfieldtype;
        this.my_afield=afield;
        this.my_what=what;
        if(this.my_gbfield==NO_GROUPING) {
            //先赋值一个，给一个开始
            //aggregator_result.put(null, 0);
            this.resultWhenNoGrouping=0;

            Type[] arrType=new Type[1];
            arrType[0]=Type.INT_TYPE;
            String[] arrName=new String[1];
            arrName[0]="aggregateval";
            this.my_TupleDesc =new TupleDesc(arrType,arrName);
        }
        else{
            Type[] arrType={gbfieldtype,Type.INT_TYPE};
            String[] arrName={"groupval","aggregateval"};
            this.my_TupleDesc =new TupleDesc(arrType,arrName);
        }



    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
        if(this.my_gbfield==NO_GROUPING)
            this.resultWhenNoGrouping++;
        else{
            if(aggregator_result.containsKey(tup.getField(this.my_gbfield)))
            {
                aggregator_result.put(tup.getField(this.my_gbfield),aggregator_result.get(tup.getField(this.my_gbfield))+1);
            }
            else
            {
                aggregator_result.put(tup.getField(this.my_gbfield),1);
            }
        }

    }

    /**
     * Create a OpIterator over group aggregate results.
     *
     * @return a OpIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public OpIterator iterator() {
        // some code goes here
        //throw new UnsupportedOperationException("please implement me for lab2");
        //如果没有分组
        ArrayList<Tuple> myTuples=new ArrayList<>();

        if(this.my_gbfield==NO_GROUPING)
        {
            Tuple newtuple=new Tuple(this.my_TupleDesc);
            IntField f=new IntField(this.resultWhenNoGrouping);
            newtuple.setField(0,f);
            myTuples.add(newtuple);
        }
        //如果有分组进入else
        else {
            //说明有分组，返回值有两个字段，开始遍历key值
            for (Field f : this.aggregator_result.keySet()) {
                //tupledesc的第一个字段TYPE是gbtype 第二个是TYPE.INT_TYPE
                Tuple newtuple=new Tuple(this.my_TupleDesc);
                //设置第一个字段 看gbfieldtype进行判断
                if(this.my_gbfieldtype==Type.INT_TYPE) {
                    //IntField newfield = (IntField) f;
                    newtuple.setField(0, (IntField) f);
                }
                else{
                    newtuple.setField(0,(StringField) f);
                }
                //获得在map里面的答案，根据键值去找
                IntField newfield=new IntField(this.aggregator_result.get(f));
                //设置第二个字段相关信息
                newtuple.setField(1,newfield);
                //将建立的这个tuple加入
                myTuples.add(newtuple);
            }
        }
        return new TupleIterator(this.my_TupleDesc,myTuples);
    //   return (OpIterator)this.myTuples.iterator();
    }

}


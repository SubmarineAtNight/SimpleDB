package simpledb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * The Catalog keeps track of all available tables in the database and their
 * associated schemas.
 * For now, this is a stub catalog that must be populated with tables by a
 * user program before it can be used -- eventually, this should be converted
 * to a catalog that reads a catalog table from disk.
 *
 * @Threadsafe
 */
public class Catalog {

    /**
     * Constructor.
     * Creates a new, empty catalog.
     */
    public ArrayList<DbFile> File=new ArrayList<>();
    public ArrayList<String> names=new ArrayList<>();
    public ArrayList<String> keys=new ArrayList<>();
    public ArrayList<Integer>  id=new ArrayList<>();
    public Catalog() {
        // some code goes here
        File=new ArrayList<>();
        names=new ArrayList<>();
        keys=new ArrayList<>();
        id=new ArrayList<>();
    }

    /**
     * Add a new table to the catalog.
     * This table's contents are stored in the specified DbFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     * @param name the name of the table -- may be an empty string.  May not be null.  If a name
     * conflict exists, use the last table to be added as the table for a given name.
     * @param pkeyField the name of the primary key field
     */
    public void addTable(DbFile file, String name, String pkeyField) {
        // some code goes here
        if (name == null) {
            throw new IllegalArgumentException("name不可为null");
        }
        for(int i=0;i<File.size();i++)
        {
            if(Objects.equals(this.names.get(i),name))
            {
                this.File.remove(i);
                this.names.remove(i);
                this.keys.remove(i);
                this.id.remove(i);
                break;
            }
            if(Objects.equals(this.id.get(i),file.getId()))
            {
                this.File.remove(i);
                this.names.remove(i);
                this.keys.remove(i);
                this.id.remove(i);
                break;
            }
        }
        File.add(file);
        names.add(name);
        keys.add(pkeyField);
        id.add(file.getId());
    }

    public void addTable(DbFile file, String name) {
        addTable(file, name, "");
    }

    /**
     * Add a new table to the catalog.
     * This table has tuples formatted using the specified TupleDesc and its
     * contents are stored in the specified DbFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     */
    public void addTable(DbFile file) {
        addTable(file, (UUID.randomUUID()).toString());
    }

    /**
     * Return the id of the table with a specified name,
     * @throws NoSuchElementException if the table doesn't exist
     */
    public int getTableId(String name) throws NoSuchElementException {
        // some code goes
        if (null == name) {
            throw new NoSuchElementException();
        }
        for(int i=0;i<this.File.size();i++)
        {
            if(Objects.equals(this.names.get(i),name))
                return this.id.get(i);
        }
        throw new NoSuchElementException("定位不到该元素");

    }

    /**
     * Returns the tuple descriptor (schema) of the specified table
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     * @throws NoSuchElementException if the table doesn't exist
     */
    public TupleDesc getTupleDesc(int tableid) throws NoSuchElementException {
        // some code goes here
        for(int i=0;i<this.File.size();i++)
        {
            if(Objects.equals(this.id.get(i),tableid))
                return this.File.get(i).getTupleDesc();
        }
        return null;
       // throw new NoSuchElementException("定位不到该元素");
    }

    /**
     * Returns the DbFile that can be used to read the contents of the
     * specified table.
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     */
    public DbFile getDatabaseFile(int tableid) throws NoSuchElementException {
        // some code goes here
        for(int i=0;i<this.File.size();i++)
        {
            if(Objects.equals(this.id.get(i),tableid))
                return this.File.get(i);
        }
        return null;
        //throw new NoSuchElementException("定位不到该元素");
    }

    public String getPrimaryKey(int tableid) {
        // some code goes here
        Map<Integer,Integer> temp=new HashMap<>();
        temp.put(1,5);
         temp.get(5);
        for(int i=0;i<this.File.size();i++)
        {
            if(Objects.equals(this.id.get(i),tableid))
                return this.keys.get(i);
        }
        return null;
        //throw new NoSuchElementException("定位不到该元素");
    }

    public Iterator<Integer> tableIdIterator() {
        // some code goes here
        return id.iterator();
       // return null;
    }

    public String getTableName(int id) {
        // some code goes here
        //return null;
        for(int i=0;i<this.File.size();i++)
        {
            if(Objects.equals(this.id.get(i),id))
                return this.names.get(i);
        }
        throw new NoSuchElementException("定位不到该元素");
    }

    /** Delete all tables from the catalog */
    public void clear() {
        //some code goes here
        this.names.clear();
        this.id.clear();
        this.File.clear();
        this.keys.clear();
    }

    /**
     * Reads the schema from a file and creates the appropriate tables in the database.
     * @param catalogFile
     */
    public void loadSchema(String catalogFile) {
        String line = "";
        String baseFolder=new File(new File(catalogFile).getAbsolutePath()).getParent();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(catalogFile)));

            while ((line = br.readLine()) != null) {
                //assume line is of the format name (field type, field type, ...)
                String name = line.substring(0, line.indexOf("(")).trim();
                //System.out.println("TABLE NAME: " + name);
                String fields = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
                String[] els = fields.split(",");
                ArrayList<String> names = new ArrayList<String>();
                ArrayList<Type> types = new ArrayList<Type>();
                String primaryKey = "";
                for (String e : els) {
                    String[] els2 = e.trim().split(" ");
                    names.add(els2[0].trim());
                    if (els2[1].trim().toLowerCase().equals("int"))
                        types.add(Type.INT_TYPE);
                    else if (els2[1].trim().toLowerCase().equals("string"))
                        types.add(Type.STRING_TYPE);
                    else {
                        System.out.println("Unknown type " + els2[1]);
                        System.exit(0);
                    }
                    if (els2.length == 3) {
                        if (els2[2].trim().equals("pk"))
                            primaryKey = els2[0].trim();
                        else {
                            System.out.println("Unknown annotation " + els2[2]);
                            System.exit(0);
                        }
                    }
                }
                Type[] typeAr = types.toArray(new Type[0]);
                String[] namesAr = names.toArray(new String[0]);
                TupleDesc t = new TupleDesc(typeAr, namesAr);
                HeapFile tabHf = new HeapFile(new File(baseFolder+"/"+name + ".dat"), t);
                addTable(tabHf,name,primaryKey);
                System.out.println("Added table : " + name + " with schema " + t);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IndexOutOfBoundsException e) {
            System.out.println ("Invalid catalog entry : " + line);
            System.exit(0);
        }
    }
}



//package simpledb;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.*;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * The Catalog keeps track of all available tables in the database and their
// * associated schemas. For now, this is a stub catalog that must be populated
// * with tables by a user program before it can be used -- eventually, this
// * should be converted to a catalog that reads a catalog table from disk.
// *
// * @Threadsafe
// */
//public class Catalog {
//
//    /**
//     * 表id到dbfile的映射
//     */
//    ConcurrentHashMap<Integer, DbFile> tableid2Dbfile;
//    /**
//     * 表id到主键名的映射
//     */
//    ConcurrentHashMap<Integer, String> tableid2pkey;
//    /**
//     * 表名到tableid的映射
//     */
//    ConcurrentHashMap<String, Integer> name2tableid;
//    /**
//     * tableid到表名的映射
//     */
//    ConcurrentHashMap<Integer, String> tableid2name;
//
//    /**
//     * Constructor. Creates a new, empty catalog.
//     */
//    public Catalog() {
//        // some code goes here
////    	finish
//        tableid2Dbfile = new ConcurrentHashMap<Integer, DbFile>(100);
//        tableid2pkey = new ConcurrentHashMap<Integer, String>(100);
//        name2tableid = new ConcurrentHashMap<String, Integer>(100);
//        tableid2name = new ConcurrentHashMap<Integer, String>(100);
//    }
//
//    /**
//     * Add a new table to the catalog. This table's contents are stored in the
//     * specified DbFile.
//     *
//     * @param file      the contents of the table to add; file.getId() is the
//     *                  identfier of this file/tupledesc param for the calls
//     *                  getTupleDesc and getFile
//     * @param name      the name of the table -- may be an empty string. May not be
//     *                  null. If a name conflict exists, use the last table to be
//     *                  added as the table for a given name.
//     * @param pkeyField the name of the primary key field
//     */
//    public void addTable(DbFile file, String name, String pkeyField) {
//        // some code goes here
////		finish
//        if (name == null) {
//            throw new IllegalArgumentException("name不可为null");
//        }
//        if (name2tableid.containsKey(name)) {
//            int oldtableid=name2tableid.get(name);
//            name2tableid.remove(name);
//            tableid2Dbfile.remove(oldtableid);
//            tableid2pkey.remove(oldtableid);
//            tableid2name.remove(oldtableid);
//        }
//        int tableid = file.getId();
//        if (tableid2Dbfile.containsKey(tableid)) {
//            String oldname=tableid2name.get(tableid);
//            tableid2Dbfile.remove(tableid);
//            tableid2name.remove(tableid);
//            tableid2pkey.remove(tableid);
//            name2tableid.remove(oldname);
//
//        }
//        tableid2Dbfile.put(tableid, file);
//        tableid2pkey.put(tableid, pkeyField);
//        name2tableid.put(name, tableid);
//        tableid2name.put(tableid, name);
//    }
//
//    public void addTable(DbFile file, String name) {
//        addTable(file, name, "");
//    }
//
//    /**
//     * Add a new table to the catalog. This table has tuples formatted using the
//     * specified TupleDesc and its contents are stored in the specified DbFile.
//     *
//     * @param file the contents of the table to add; file.getId() is the identfier
//     *             of this file/tupledesc param for the calls getTupleDesc and
//     *             getFile
//     */
//    public void addTable(DbFile file) {
//        addTable(file, (UUID.randomUUID()).toString());
//    }
//
//    /**
//     * Return the id of the table with a specified name,
//     *
//     * @throws NoSuchElementException if the table doesn't exist
//     */
//    public int getTableId(String name) throws NoSuchElementException {
//        // some code goes here
////		finish
//        if (null == name || !name2tableid.containsKey(name)) {
//            throw new NoSuchElementException();
//        }
//        return name2tableid.get(name);
//    }
//
//    /**
//     * Returns the tuple descriptor (schema) of the specified table
//     *
//     * @param tableid The id of the table, as specified by the DbFile.getId()
//     *                function passed to addTable
//     * @throws NoSuchElementException if the table doesn't exist
//     */
//    public TupleDesc getTupleDesc(int tableid) throws NoSuchElementException {
//        // some code goes here
////		finish
//
//        return tableid2Dbfile.get(tableid).getTupleDesc();
//    }
//
//    /**
//     * Returns the DbFile that can be used to read the contents of the specified
//     * table.
//     *
//     * @param tableid The id of the table, as specified by the DbFile.getId()
//     *                function passed to addTable
//     */
//    public DbFile getDatabaseFile(int tableid) throws NoSuchElementException {
//        // some code goes here
////		finish
//        return tableid2Dbfile.get(tableid);
//    }
//
//    public String getPrimaryKey(int tableid) {
//        // some code goes here
////		finish
//        return tableid2pkey.get(tableid);
//    }
//
//    public Iterator<Integer> tableIdIterator() {
//        // some code goes here
////		finish
//        return tableid2name.keySet().iterator();
//    }
//
//    public String getTableName(int id) {
//        // some code goes here
////		finish
//        return tableid2name.get(id);
//    }
//
//    /** Delete all tables from the catalog */
//    public void clear() {
//        // some code goes here
////		finish
//        tableid2Dbfile.clear();
//        tableid2name.clear();
//        tableid2pkey.clear();
//        name2tableid.clear();
//    }
//
//    /**
//     * Reads the schema from a file and creates the appropriate tables in the
//     * database.
//     *
//     * @param catalogFile
//     */
//    public void loadSchema(String catalogFile) {
//        String line = "";
//        String baseFolder = new File(new File(catalogFile).getAbsolutePath()).getParent();
//        try {
//            BufferedReader br = new BufferedReader(new FileReader(new File(catalogFile)));
//
//            while ((line = br.readLine()) != null) {
//                // assume line is of the format name (field type, field type, ...)
//                String name = line.substring(0, line.indexOf("(")).trim();
//                // System.out.println("TABLE NAME: " + name);
//                String fields = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
//                String[] els = fields.split(",");
//                ArrayList<String> names = new ArrayList<String>();
//                ArrayList<Type> types = new ArrayList<Type>();
//                String primaryKey = "";
//                for (String e : els) {
//                    String[] els2 = e.trim().split(" ");
//                    names.add(els2[0].trim());
//                    if (els2[1].trim().toLowerCase().equals("int"))
//                        types.add(Type.INT_TYPE);
//                    else if (els2[1].trim().toLowerCase().equals("string"))
//                        types.add(Type.STRING_TYPE);
//                    else {
//                        System.out.println("Unknown type " + els2[1]);
//                        System.exit(0);
//                    }
//                    if (els2.length == 3) {
//                        if (els2[2].trim().equals("pk"))
//                            primaryKey = els2[0].trim();
//                        else {
//                            System.out.println("Unknown annotation " + els2[2]);
//                            System.exit(0);
//                        }
//                    }
//                }
//                Type[] typeAr = types.toArray(new Type[0]);
//                String[] namesAr = names.toArray(new String[0]);
//                TupleDesc t = new TupleDesc(typeAr, namesAr);
//                HeapFile tabHf = new HeapFile(new File(baseFolder + "/" + name + ".dat"), t);
//                addTable(tabHf, name, primaryKey);
//                System.out.println("Added table : " + name + " with schema " + t);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.exit(0);
//        } catch (IndexOutOfBoundsException e) {
//            System.out.println("Invalid catalog entry : " + line);
//            System.exit(0);
//        }
//    }
//}
package simpledb;

import java.io.IOException;
import java.util.*;

/**
 * SeqScan is an implementation of a sequential scan access method that reads
 * each tuple of a table in no particular order (e.g., as they are laid out on
 * disk).
 */
public class SeqScan implements OpIterator {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a sequential scan over the specified table as a part of the
     * specified transaction.
     *
     * @param tid
     *            The transaction this scan is running as a part of.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    private TransactionId mytid;
    private int mytableid;
    private String mytableAlias;
    private  DbFileIterator mydbFileIt;

    public SeqScan(TransactionId tid, int tableid, String tableAlias) {
        // some code goes here
        this.mytid=tid;
        this.mytableid=tableid;
        this.mytableAlias=tableAlias;
        this.mydbFileIt=Database.getCatalog().getDatabaseFile(this.mytableid).iterator(this.mytid);
    }

    /**
     * @return
     *       return the table name of the table the operator scans. This should
     *       be the actual name of the table in the catalog of the database
     * */
    public String getTableName() {
        return Database.getCatalog().getTableName(this.mytableid);
    }

    /**
     * @return Return the alias of the table this operator scans.
     * */
    public String getAlias()
    {
        // some code goes here
        return mytableAlias;
    }

    /**
     * Reset the tableid, and tableAlias of this operator.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    public void reset(int tableid, String tableAlias) {
        // some code goes here
        this.mytableid=tableid;
        this.mytableAlias=tableAlias;
    }

    public SeqScan(TransactionId tid, int tableId) {
        this(tid, tableId, Database.getCatalog().getTableName(tableId));
    }

    public void open() throws DbException, TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
      this.mydbFileIt.open();

    }

    /**
     * Returns the TupleDesc with field names from the underlying HeapFile,
     * prefixed with the tableAlias string from the constructor. This prefix
     * becomes useful when joining tables containing a field(s) with the same
     * name.  The alias and name should be separated with a "." character
     * (e.g., "alias.fieldName").
     *
     * @return the TupleDesc with field names from the underlying HeapFile,
     *         prefixed with the tableAlias string from the constructor.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        TupleDesc myTy= Database.getCatalog().getTupleDesc(this.mytableid);
        Type[] typeAr=new Type[myTy.numFields()];
        String[] fieldAr=new String[myTy.numFields()];
        for(int i=0;i<myTy.numFields();i++)
        {
           typeAr[i]=myTy.TD.get(i).fieldType;
           String prefix=(this.getAlias()==null|| this.getAlias().equals(""))?"null":this.getAlias();
           String earlyName=myTy.TD.get(i).fieldName;
           String laterName=(earlyName==null|| earlyName.equals(""))?"null":earlyName;
           fieldAr[i]=prefix+"."+laterName;
        }
        TupleDesc myTd=new TupleDesc(typeAr,fieldAr);
        return myTd;
    }


    public boolean hasNext() throws TransactionAbortedException, DbException, IOException, InterruptedException {
        // some code goes here
        return this.mydbFileIt.hasNext();
    }

    public Tuple next() throws NoSuchElementException,
            TransactionAbortedException, DbException, IOException, InterruptedException {
        // some code goes here
        return this.mydbFileIt.next();
    }

    public void close() {
        // some code goes here
        this.mydbFileIt.close();
    }

    public void rewind() throws DbException, NoSuchElementException,
            TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
        this.mydbFileIt.rewind();
    }
}

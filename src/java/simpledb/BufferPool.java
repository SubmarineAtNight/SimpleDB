package simpledb;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 *
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    //负责保持状态关于事务、锁的类型0代表读，1代表写。还有相应的pageId ,然后弄成一个class数组存放着 之后就通过遍历的方式 去找
    class PageLock{
        public static final int SHARE = 0;
        public static final int EXCLUSIVE = 1;
        private TransactionId tid;
        private int type;

        public PageLock(TransactionId tid, int type){
            this.tid =  tid;
            this.type = type;
        }

        public TransactionId getTid() {
            return tid;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    class LockManager{
        private  ConcurrentHashMap<PageId, ConcurrentHashMap<TransactionId,PageLock>> lockMap;
        // dependency graphs
        private ConcurrentHashMap<TransactionId,ConcurrentHashMap<TransactionId,TransactionId>> dpgraphs;
        LockManager(){
            lockMap=new ConcurrentHashMap<>();
            dpgraphs=new ConcurrentHashMap<>();
        }
        //获得锁
        public synchronized boolean acquireLock(PageId pageId, TransactionId tid, int requiredType) throws InterruptedException, TransactionAbortedException {
            //页面上没有任何事务，不论什么请求都通过
            if(lockMap.get(pageId) == null){
                PageLock pageLock = new PageLock(tid,requiredType);
                ConcurrentHashMap<TransactionId,PageLock> pageLocks = new ConcurrentHashMap<>();
                pageLocks.put(tid,pageLock);
                lockMap.put(pageId,pageLocks);
                //System.out.println(thread + ": the " + pageId + " have no lock, transaction" + tid + " require " + lockType + ", accept");
                return true;
            }
            //页面上有事务

            //如果上面的事务都不是当前事务
            ConcurrentHashMap<TransactionId,PageLock> PageLocks=lockMap.get(pageId);
            if(PageLocks.get(tid)==null){
                boolean isallread = true;
                if(PageLocks.size()==1)
                {
                    PageLock curLock = null;
                    for (PageLock lock : PageLocks.values()){
                        curLock = lock;
                    }
                    if(curLock.getType()==PageLock.EXCLUSIVE)
                        isallread=false;
                }

                //如果都是读
                if(isallread){
                    //要获取的也是读ok
                    if(requiredType==0)
                    {
                        PageLock p=new PageLock(tid,requiredType);
                        PageLocks.put(tid,p);
                        lockMap.put(pageId,PageLocks);
                        return true;
                    }
                    //要获取的是写，就得wait
                    //此处可以判断是否存在死锁 别人读我们写
                    for(PageLock lock : PageLocks.values()){
                        if(isPathExists(lock.tid,tid)){
                        //if(dpgraphs.get(lock.tid)!=null&&dpgraphs.get(lock.tid).get(tid)!=null){
                            throw new TransactionAbortedException();
                            }
                        else
                        {
                            //重新进行修改
                            if(dpgraphs.containsKey(tid)){
                                ConcurrentHashMap<TransactionId,TransactionId> p=dpgraphs.get(tid);
                                p.put(lock.tid,lock.tid);
                                dpgraphs.put(tid,p);
                            }
                            else{
                                ConcurrentHashMap<TransactionId,TransactionId>  p=new ConcurrentHashMap<>();
                                p.put(lock.tid,lock.tid);
                                dpgraphs.put(tid,p);
                            }
                        }
                    }

                    wait(10);
                    return false;
                }
                else{
                    //此处可以判断是否存在死锁 别人有写 而且可以推断一次只会存在一个写 不可能同时在一个页面上有写的操作
                    for(PageLock lock : PageLocks.values()){
                        if(isPathExists(lock.tid,tid)){
                        //if(dpgraphs.get(lock.tid)!=null&&dpgraphs.get(lock.tid).get(tid)!=null) {
                            throw new TransactionAbortedException();
                        }
                        else
                        {
                            //重新进行修改
                            if(dpgraphs.containsKey(tid)){
                                ConcurrentHashMap<TransactionId,TransactionId> p=dpgraphs.get(tid);
                                p.put(lock.tid,lock.tid);
                                dpgraphs.put(tid,p);
                            }
                            else{
                                ConcurrentHashMap<TransactionId,TransactionId>  p=new ConcurrentHashMap<>();
                                p.put(lock.tid,lock.tid);
                                dpgraphs.put(tid,p);
                            }
                        }
                    }
                    //如果有写了就得等待
                    wait(10);
                    return false;
                }
            }

            //如果页面有事务tid的锁
            else {
                //如果写入想要成立唯一的可能就是只有一个而且是
                //如果本身这个是写入 那么不论怎样都可以
                if(PageLocks.get(tid).getType()==1&&PageLocks.size() == 1){
                  //  PageLock p=new PageLock(tid,requiredType);
                    //PageLocks.put(tid,p);
                    //lockMap.put(pageId,PageLocks);
                    return true;
                }
                if(requiredType==1) {
                    //升级，也是唯一write的情况
                    if (PageLocks.size() == 1 && PageLocks.get(tid).getType() == 0) {
                        PageLock pagelock = PageLocks.get(tid);
                        pagelock.setType(1);
                        lockMap.get(pageId).put(tid, pagelock);
                       // PageLocks.put(tid,pagelock);
                        return true;
                    }
                    if(PageLocks.size()>1){
                        //此处可以判断是否存在死锁 //我们写对方读
                        for(PageLock lock : PageLocks.values()){
                            if(isPathExists(lock.tid,tid)){
                           // if(lock.tid!=tid&&dpgraphs.get(lock.tid)!=null&&dpgraphs.get(lock.tid).get(tid)!=null) {
                                throw new TransactionAbortedException();
                            }
                            else if(lock.tid!=tid)
                            {
                                //重新进行修改
                                if(dpgraphs.containsKey(tid)){
                                    ConcurrentHashMap<TransactionId,TransactionId> p=dpgraphs.get(tid);
                                    p.put(lock.tid,lock.tid);
                                    dpgraphs.put(tid,p);
                                }
                                else{
                                    ConcurrentHashMap<TransactionId,TransactionId>  p=new ConcurrentHashMap<>();
                                    p.put(lock.tid,lock.tid);
                                    dpgraphs.put(tid,p);
                                }
                            }
                        }
                        //抛出异常
                        wait(10);
                        return false;
                       // throw new TransactionAbortedException();
                    }
                }
                //本身这个是读 而且需求的也是读 该怎么做呢  应该是可以
                return true;
            }
           // return true;
        }

        boolean isPathExists(TransactionId source,TransactionId destination){
            Queue<TransactionId> q = new LinkedList<>();
            q.offer(source);
            while(!q.isEmpty())
            {
                TransactionId tid=q.peek();
                if(tid==destination)
                    return true;
                q.poll();
                if(dpgraphs.get(tid)==null)
                    return false;
                for(TransactionId t:dpgraphs.get(tid).keySet()){
                    if(t==destination)
                        return true;
                    q.offer(t);
                }
            }
            return false;
        }

        //判断指定事务是否持有某一page上的锁
        public synchronized boolean isholdLock(TransactionId tid, PageId pid){
            ConcurrentHashMap<TransactionId,PageLock> pageLocks;
            pageLocks = lockMap.get(pid);
            if(pageLocks == null){
                return false;
            }
            PageLock pageLock = pageLocks.get(tid);
            if(pageLock == null){
                return false;
            }
            return true;
        }
        //释放指定事务在某一page上的所有锁
        public synchronized boolean releaseLock(TransactionId tid, PageId pid){
            if (isholdLock(tid,pid)){
                ConcurrentHashMap<TransactionId,PageLock> pageLocks = lockMap.get(pid);
                pageLocks.remove(tid);
                if (pageLocks.size() == 0){
                    lockMap.remove(pid);
                }
                this.notifyAll();
                return true;
            }
            return false;
        }
        //将该事务commit 释放所有的锁
        public synchronized void transactionComplete(TransactionId tid){
            Set<PageId> pageIds = lockMap.keySet();
            for (PageId pageId : pageIds){
                releaseLock(tid,pageId);
            }
            //ConcurrentHashMap<TransactionId,ConcurrentHashMap<TransactionId,TransactionId>> dpgraphs;
            //释放依赖该锁的所有线 （能到他说明他已经没有依赖的了，否则执行不了）
            for(TransactionId Tid:dpgraphs.keySet()){
                if(Tid!=tid&&dpgraphs.get(Tid)!=null&&dpgraphs.get(Tid).get(tid)!=null){
                    ConcurrentHashMap<TransactionId,TransactionId> p=dpgraphs.get(Tid);
                    //接触T对当前被完成的事务的依赖
                    p.remove(tid);
                    //更新dpgraphs
                    dpgraphs.put(Tid,p);
                }
            }
        }
        //将该事务的页面全部刷新到磁盘
        public  synchronized void flushallPages(TransactionId tid) throws IOException {
            Set<PageId> pageIds=lockMap.keySet();
            for(PageId pageId:pageIds){
                //该页面属于当前事务
                if(lockMap.get(pageId).get(tid)!=null){
                    flushPage(pageId);
                }
            }
        }
        public  synchronized void updatepagesFromDisk(TransactionId tid){
            Set<PageId> pageIds=lockMap.keySet();
            for(PageId pageId:pageIds){
                if(lockMap.get(pageId).get(tid)!=null){
                    DbFile dbFile = Database.getCatalog().getDatabaseFile(pageId.getTableId());
                    Page page = dbFile.readPage(pageId);
                    buffer.put(pageId.hashCode(),page);
                    //markdirty此时可以绕开事务检测了 因为是直接从内部 我们可以用buffer 这是不公开的
                    buffer.get(pageId.hashCode()).markDirty(false,null);
                }
            }
        }
    }
    /** Bytes per page, including header. */
    private static final int DEFAULT_PAGE_SIZE = 4096;

    private static int pageSize = DEFAULT_PAGE_SIZE;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */

    private int numPages;
    //Integer pid.hashcode
    private Map<Integer,Page> buffer;
    ArrayList<Page> array_Pages;
    private LockManager lockManager;
    //ArrayList<stage> array_stages;
    private Page PageToEvict;
    public BufferPool(int numPages) {
        this.numPages=numPages;
        array_Pages=new ArrayList<>();
       // buffer=new HashMap<Integer, Page>();
        //buffer=new LinkedHashMap<Integer,Page>();
        buffer=new ConcurrentHashMap<Integer,Page>();
        this.PageToEvict=null;
        lockManager=new LockManager();
        // some code goes here
    }

    public static int getPageSize() {
      return pageSize;
    }

    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void setPageSize(int pageSize) {
    	BufferPool.pageSize = pageSize;
    }

    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void resetPageSize() {
    	BufferPool.pageSize = DEFAULT_PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, a page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
            throws TransactionAbortedException, DbException, IOException, InterruptedException {
        //不断去锁
        int lockType;
        if (perm == Permissions.READ_ONLY){
            lockType = PageLock.SHARE;
        } else {
            lockType = PageLock.EXCLUSIVE;
        }
        long st = System.currentTimeMillis();
        boolean isacquired = false;
        while(!isacquired){

            try {
                isacquired = lockManager.acquireLock(pid,tid,lockType);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();
            if(now - st > 500){
                throw new TransactionAbortedException();
            }
        }

        if (!this.buffer.containsKey(pid.hashCode())) {
            DbFile dbFile = Database.getCatalog().getDatabaseFile(pid.getTableId());
            Page page = dbFile.readPage(pid);
            if (buffer.size() >= numPages) {
                evictPage();
            }
            buffer.put(pid.hashCode(), page);
        }
        return this.buffer.get(pid.hashCode());
        // return null;
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public void releasePage(TransactionId tid, PageId pid) {
        // some code goes here
        lockManager.releaseLock(tid,pid);
        // not necessary for lab1|lab2
      //  lockManager.
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
       //lockManager.transactionComplete(tid);
        transactionComplete(tid, true) ;
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId p) {
        // some code goes here
        // not necessary for lab1|lab2
        return  lockManager.isholdLock(tid,p);
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    //
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        // some code goes here
        // not necessary for lab1|lab2

        //commit
        if(commit){
           // lockManager.flushallPages(tid);
            try {
                flushPages(tid);
            } catch (IOException | CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        else{
           // lockManager.updatepagesFromDisk(tid);
            storePages(tid);
        }
        //释放有关事务的锁
        lockManager.transactionComplete(tid);

    }

    /**
     * Add a tuple to the specified table on behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to and any other
     * pages that are updated (Lock acquisition is not needed for lab2).
     * May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have
     * been dirtied to the cache (replacing any existing versions of those pages) so
     * that future requests see up-to-date pages.
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
            throws DbException, IOException, TransactionAbortedException, InterruptedException {
        // some code goes here
        // not necessary for lab1
        //HeapFile heapFile=(HeapFile)Database.getCatalog().getDatabaseFile(tableId);
        DbFile heapFile=Database.getCatalog().getDatabaseFile(tableId);
        //heapFile.insertTuple(tid,t);
        updateBufferpoll(heapFile.insertTuple(tid, t),tid);
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from and any
     * other pages that are updated. May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have
     * been dirtied to the cache (replacing any existing versions of those pages) so
     * that future requests see up-to-date pages.
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public  void deleteTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException, InterruptedException {
        // some code goes here
        // not necessary for lab1
        DbFile heapFile=Database.getCatalog().getDatabaseFile(t.getRecordId().getPageId().getTableId());
        updateBufferpoll(heapFile.deleteTuple(tid, t),tid);
    }

    public void updateBufferpoll(ArrayList<Page> array,TransactionId tid) throws DbException, IOException {
        for (Page p : array) {
            //还没有被更新到磁盘所以设置为脏页dirty page 并且把这个插入或者删除tuple的页给放到bufferpool里面
            p.markDirty(true,tid);
            if(buffer.size()>numPages)
                evictPage();
            buffer.put(p.getId().hashCode(),p);
        }
    }


    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        // some code goes here
        // not necessary for lab1

    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.

        Also used by B+ tree files to ensure that deleted pages
        are removed from the cache so they can be reused safely
    */
    public synchronized void discardPage(PageId pid) {
        // some code goes here
        // not necessary for lab1
        if(buffer.containsKey(pid.hashCode()))
        buffer.remove(pid.hashCode());
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized  void flushPage(PageId pid) throws IOException {
        // some code goes here
        // not necessary for lab1
        //写回到磁盘里
        Database.getCatalog().getDatabaseFile(pid.getTableId()).writePage(buffer.get(pid.hashCode()));
        //检查如果页面本身不存在就过
        if(buffer.get(pid.hashCode())!=null) {
           // buffer.get(pid.hashCode()).markDirty(false,null);
            Page p = buffer.get(pid.hashCode());
            p.markDirty(false,null);
            buffer.put(p.hashCode(),p);
        }
    }
    /**
     * 使用对象的序列化进而实现深拷贝
     * @param obj
     * @param <T>
     * @return
     */
    /** Write all pages of the specified transaction to disk.
     */
    public synchronized  void flushPages(TransactionId tid) throws IOException, CloneNotSupportedException {
        // some code goes here
        // not necessary for lab1|lab2
        Map<Integer,Page> cloneM= new ConcurrentHashMap<>(buffer);
        Set<Integer> pagesHash= cloneM.keySet();
        Object[] it=  pagesHash.toArray();
        for(int j=0;j<it.length;j++){
            Page p=cloneM.get((Integer)it[j]);
            //if(p==null)return ;
            if(tid.equals(p.isDirty()))
                flushPage(p.getId());
        }
//        for(int hashcode:buffer.keySet()){
//            Page p=buffer.get(hashcode);
//            if(tid.equals(p.isDirty()))
//                flushPage(p.getId());
//        }
//        for(Integer key:buffer.keySet()){
//            Page p=buffer.get(key);
//            if(tid.equals(p.isDirty()));
//            flushPage(p.getId());
//        }

    }

    public synchronized void storePages(TransactionId tid){
        Map<Integer,Page> cloneM= new ConcurrentHashMap<Integer,Page>(buffer);
        Set<Integer> pagesHash= cloneM.keySet();
        //Set<Integer> pagesHash= buffer.keySet();
        Object[] it=  pagesHash.toArray();
        for(int j=0;j<it.length;j++){
            Page p=cloneM.get((Integer)it[j]);
            //if(p==null)return ;
            if(tid.equals(p.isDirty()))
            {
                Page pagefromdisk=Database.getCatalog().getDatabaseFile(p.getId().getTableId()).readPage(p.getId());
                //buffer.put(p.hashCode(),pagefromdisk);
                //buffer.get(p.hashCode()).markDirty(false,null);
                pagefromdisk.markDirty(false,null);
                buffer.put(p.hashCode(),pagefromdisk);
            }
        }
//        for(Integer key:buffer.keySet()){
//            Page p=buffer.get(key);
//            if(tid.equals(p.isDirty()))
//            {
//                Page pagefromdisk=Database.getCatalog().getDatabaseFile(p.getId().getTableId()).readPage(p.getId());
//                //buffer.put(p.hashCode(),pagefromdisk);
//                //buffer.get(p.hashCode()).markDirty(false,null);
//                pagefromdisk.markDirty(false,null);
//                buffer.put(p.hashCode(),pagefromdisk);
//            }
//        }

//        for(int hashcode:buffer.keySet()){
//            Page p=buffer.get(hashcode);
//            if(tid.equals(p.isDirty()))
//            {
//                Page pagefromdisk=Database.getCatalog().getDatabaseFile(p.getId().getTableId()).readPage(p.getId());
//                //buffer.get(p.hashCode()).markDirty(false,null);
//                pagefromdisk.markDirty(false,null);
//                buffer.put(p.hashCode(),pagefromdisk);
//            }
//        }
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized  void evictPage() throws DbException, IOException {
        // some code goes here
        // not necessary for lab1
        //策略就是
        //如果是脏页就需要刷盘

        //因为要插入新的页但是已经满了所以必须从缓存区清除脏页
        Map<Integer,Page> cloneM= new ConcurrentHashMap<Integer,Page>(buffer);
        Set<Integer> pagesHash= cloneM.keySet();
        //Set<Integer> pagesHash= buffer.keySet();
        Object[] it=  pagesHash.toArray();
        for(int j=0;j<it.length;j++){
            if(cloneM.get(it[j]).isDirty()==null){
                PageToEvict=cloneM.get(it[j]);
                discardPage(PageToEvict.getId());
                return ;
            }
        }

//        for(Integer key:buffer.keySet()){
//            if(buffer.get(key).isDirty()==null){
//                PageToEvict=buffer.get(key);
//                discardPage(PageToEvict.getId());
//                return ;
//            }
//        }

//        this.PageToEvict=null;
//        for(int hashcode:buffer.keySet())
//        {
//            if(buffer.get(hashcode).isDirty()==null){
//                PageToEvict=buffer.get(hashcode);
//                discardPage(PageToEvict.getId());
//                return ;
//            }
//        }
        throw new DbException("all pages are dirty page ");
        //no steal政策 不允许摘掉脏页
//        if(this.PageToEvict.isDirty()!=null){
//            flushPage(this.PageToEvict.getId());
        //this.PageToEvict.markDirty(false,PageToEvict.isDirty());
//        }

    }

}





































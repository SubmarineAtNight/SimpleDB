package simpledb;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     * 
     * @param t
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
    private TransactionId tid;
    private OpIterator child;
    boolean havedeleted;

    public Delete(TransactionId t, OpIterator child) {
        // some code goes here
        this.tid=t;
        this.child=child;
        havedeleted=false;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        ArrayList<Type> array1=new ArrayList<>();
        array1.add(Type.INT_TYPE);
        ArrayList<String> array2=new ArrayList<>();
        array2.add(null);
        Type[] t = new Type[0];
        String[] s = new String[0];
        TupleDesc tupledesc=new TupleDesc(array1.toArray(t),array2.toArray(s));
        return tupledesc;
    }

    public void open() throws DbException, TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
        child.open();
        super.open();
    }

    public void close() throws IOException, TransactionAbortedException, DbException, InterruptedException {
        // some code goes here
        child.open();
        super.close();
    }

    public void rewind() throws DbException, TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
        child.rewind();
        havedeleted=true;
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     * 
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException, IOException, InterruptedException {
        // some code goes here
        if(!havedeleted)
        {
            havedeleted=true;
            int count=0;
            while(child.hasNext())
            {
                Database.getBufferPool().deleteTuple(tid,child.next());
                count++;
            }
            Tuple t=new Tuple(this.getTupleDesc());
            t.setField(0,new IntField(count));
            return t;
        }
        return null;
    }

    @Override
    public OpIterator[] getChildren() {
        // some code goes here
        return new OpIterator[]{child};
    }

    @Override
    public void setChildren(OpIterator[] children) {
        // some code goes here
        child=children[0];
    }

}

package simpledb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */
   private int gbfield;
   private Type gbfieldtype;
   private int afield;
   private Op what;

   private TupleDesc tupledesc;
   private AggregateChoice agc;

    private abstract class AggregateChoice{
        //最后的结果
        protected Map<Field,Integer> aggregator_result;
        //构造函数
        AggregateChoice(){
            aggregator_result=new HashMap<>();
        }
        //给一个函数来处理究竟是选择什么类型的聚合，不同的规则
       public abstract void mergeTuple(Tuple tup,int gbfield,int afield);
        //返回最终结果
        public Map<Field,Integer> getResultMap(){
            return aggregator_result;
        }
    }

    private class Count_Aggregate extends AggregateChoice{
        Count_Aggregate(){super();}
        @Override
        public void mergeTuple(Tuple tup, int gbfield, int afield) {
            Field gbField=(gbfield==-1)?null:tup.getField(gbfield);
            if(aggregator_result.containsKey(gbField))
                aggregator_result.put(gbField,aggregator_result.get(gbField)+1);
            else
                aggregator_result.put(gbField,1);
        }
    }

    private class Sum_Aggregate extends  AggregateChoice{
        Sum_Aggregate(){super();}
        @Override
        public void mergeTuple(Tuple tup, int gbfield, int afield) {
            Field gbField=(gbfield==-1)?null:tup.getField(gbfield);
            IntField Infield=(IntField)tup.getField(afield);
            if(aggregator_result.containsKey(gbField))
                aggregator_result.put(gbField,aggregator_result.get(gbField)+Infield.getValue());
            else
                aggregator_result.put(gbField,Infield.getValue());
        }
    }

    private class AVG_Aggregate extends  AggregateChoice{
        Map<Field,Integer> Count=new HashMap<>();
        Map<Field,Integer> Sum=new HashMap<>();

        AVG_Aggregate(){super();}
        @Override
        public void mergeTuple(Tuple tup, int gbfield, int afield) {
            Field gbField=(gbfield==-1)?null:tup.getField(gbfield);
            IntField Infield=(IntField)tup.getField(afield);
            if(aggregator_result.containsKey(gbField)){
                Sum.put(gbField,Sum.get(gbField)+Infield.getValue());
                Count.put(gbField,Count.get(gbField)+1);
                aggregator_result.put(gbField,Sum.get(gbField)/Count.get(gbField));
            }
            else{
                Sum.put(gbField,Infield.getValue());
                Count.put(gbField,1);
                aggregator_result.put(gbField,Sum.get(gbField)/Count.get(gbField));
            }
        }
    }

    private class MIN_Aggregate extends AggregateChoice{
        MIN_Aggregate(){super();}
        @Override
        public void mergeTuple(Tuple tup, int gbfield, int afield) {
            Field gbField=(gbfield==-1)?null:tup.getField(gbfield);
            IntField Infield=(IntField)tup.getField(afield);
            if(aggregator_result.containsKey(gbField)&&Infield.getValue()<aggregator_result.get(gbField))
                aggregator_result.put(gbField,Infield.getValue());
            else if(!aggregator_result.containsKey(gbField))
                aggregator_result.put(gbField,Infield.getValue());
        }
    }

    private class MAX_Aggregate extends AggregateChoice{
        MAX_Aggregate(){super();}
        @Override
        public void mergeTuple(Tuple tup, int gbfield, int afield) {
            Field gbField=(gbfield==-1)?null:tup.getField(gbfield);
            IntField Infield=(IntField)tup.getField(afield);
            if(aggregator_result.containsKey(gbField)&&Infield.getValue()>aggregator_result.get(gbField))
                aggregator_result.put(gbField,Infield.getValue());
            else if(!aggregator_result.containsKey(gbField))
                aggregator_result.put(gbField,Infield.getValue());
        }
    }

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
        this.gbfield=gbfield;
        this.gbfieldtype=gbfieldtype;
        this.afield=afield;
        this.what=what;

        if(gbfield==NO_GROUPING) {
            Type[] arr1={Type.INT_TYPE};
            String[] arr2={"aggregateval"};
            tupledesc=new TupleDesc(arr1,arr2);
        }
        else {
            Type[] arr1={gbfieldtype,Type.INT_TYPE};
            String[] arr2={"groupval","aggregateval"};
            tupledesc=new TupleDesc(arr1,arr2);
        }

        switch (what)
        {
            case COUNT:
                this.agc=new Count_Aggregate();
                break;
            case SUM:
                this.agc=new Sum_Aggregate();
                break;
            case AVG:
                this.agc=new AVG_Aggregate();
                break;
            case MIN:
                this.agc=new MIN_Aggregate();
                break;
            case MAX:
                this.agc=new MAX_Aggregate();
                break;
        }

    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
        this.agc.mergeTuple(tup,this.gbfield,this.afield);
    }

    /**
     * Create a OpIterator over group aggregate results.
     * 
     * @return a OpIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public OpIterator iterator() {
        // some code goes here
        //throw new
        //UnsupportedOperationException("please implement me for lab2");
        ArrayList<Tuple> tuples=new ArrayList<>();
        Map<Field,Integer> aggregage_result=this.agc.aggregator_result;
        if(this.gbfield==NO_GROUPING)
        {
            Tuple newtuple=new Tuple(this.tupledesc);
            //将数据封装到一个整型字段类里
            IntField field=new IntField(aggregage_result.get(null));
            newtuple.setField(0,field);
            tuples.add(newtuple);
        }
        else {
            for(Field f:aggregage_result.keySet()){
                Tuple newtuple=new Tuple(this.tupledesc);
                newtuple.setField(0,f);
                //aggregateval是整数 所以自然封装到整型字段里面去
                IntField field=new IntField(aggregage_result.get(f));
                newtuple.setField(1,field);
                tuples.add(newtuple);
            }
        }
        return new TupleIterator(this.tupledesc,tuples);
    }

}

package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 *
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    /**
     * Constructs a heap file backed by the specified file.
     *
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    private final File f;
    private  final TupleDesc Tu;
    //private Object PageId;

    //numPages存储的是所需要的page的数量
    //private int numPages;
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
        this.f=f;
        this.Tu=td;
        //numPages = (int)Math.ceil((double)f.length() / (double)BufferPool.getPageSize());
    }

    /**
     * Returns the File backing this HeapFile on disk.
     *
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return this.f;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere to ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     *
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return this.getFile().getAbsoluteFile().hashCode();
        //throw new UnsupportedOperationException("implement this");
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     *
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
       // throw new UnsupportedOperationException("implement this");
        return this.Tu;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        Page newPage = null;
        byte[] data = new byte[BufferPool.getPageSize()];
        try {
            RandomAccessFile accessFile = new RandomAccessFile(f, "r");
            accessFile.seek(pid.getPageNumber() * BufferPool.getPageSize());
            accessFile.read(data, 0, data.length);
            newPage = new HeapPage((HeapPageId) pid, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newPage;

    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
        if(page.getId().getPageNumber()>numPages())
            throw new IllegalArgumentException();
        RandomAccessFile accessFile=new RandomAccessFile(f,"rw");
        accessFile.seek(page.getId().getPageNumber() * BufferPool.getPageSize());
        accessFile.write(page.getPageData());
        accessFile.close();
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        //return this.numPages;
        // 文件长度 / 每页的字节数            1.5 / 1 = 1 2个 还可以往第二页加的 现在的情况就是不找了然后直接加一个新的..
        int res = (int) Math.floor(f.length() * 1.0 / BufferPool.getPageSize());
        return res;
    }
    //插入了一个后返回了一个被插入的 但file如果没有被修改 那么再次插入的时候不是又会是原本的情况了么 因为heapfile在insert会从file中把page中读出来进行操作 不write的话怎么可以呢
    //插入后 heapfile
   //  see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException, InterruptedException {
        // some code goes here
        //return null;
        ArrayList<Page> array_List=new ArrayList<>();

        for(int i=0;i<this.numPages();i++)
        {
            HeapPageId id=new HeapPageId(this.getId(),i);
            //写法的差异是为了信息安全？
            //HeapPage newPage=(HeapPage)this.readPage(id);
            HeapPage newPage=(HeapPage) Database.getBufferPool().getPage(tid, id, Permissions.READ_WRITE);
            if(newPage.getNumEmptySlots()!=0){
                newPage.insertTuple(t);
                array_List.add(newPage);
               // writePage(newPage);
                return array_List;
            }
            else{
                //完善的内容，当该page上没有空slot时，释放该page上的锁，避免影响其他事务的访问
                Database.getBufferPool().releasePage(tid,id);
                continue;
            }
        }
        //需要增加新的空间了在磁盘上
        // 如果所有页都已经写满，就要新建新的页面来加入(记得开启 append = true 也就是增量增加)
        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(f, true));
        // 新建一个空的页
        byte[] emptyPage = HeapPage.createEmptyPageData();
        output.write(emptyPage);
        // close 前会调用 flush() 刷盘到文件
        output.close();
            HeapPageId id=new HeapPageId(this.getId(),numPages()-1);
           // HeapPage newPage=new HeapPage(id,emptyPage);
        //加载到缓存
            HeapPage newPage=(HeapPage) Database.getBufferPool().getPage(tid,id,Permissions.READ_WRITE);
            newPage.insertTuple(t);
            array_List.add(newPage);
            //writePage(newPage);

        return array_List;
        // not necessary for lab1
    }


    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException, IOException, InterruptedException {
        // some code goes here
        ArrayList<Page> array_List=new ArrayList<>();
        HeapPageId id=new HeapPageId(this.getId(),t.getRecordId().getPageId().getPageNumber());
        Page newPage=(HeapPage)Database.getBufferPool().getPage(tid, id, Permissions.READ_WRITE);
        ((HeapPage) newPage).deleteTuple(t);
        array_List.add(newPage);
        //writePage(newPage);
        return array_List;

        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
        return new DbI(tid);
    }

    @Override
    public DbFileIterator reverseiterator(TransactionId tid) {
        return null;
    }

    private class DbI implements DbFileIterator{
        TransactionId mid;
        int posOfPage;
        Iterator<Tuple> it;
        HeapPage mypHeapPage;

        public DbI(TransactionId tid)
        {
            this.mid=tid;
            posOfPage=0;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException, IOException, InterruptedException {
            mypHeapPage = (HeapPage) Database.getBufferPool().getPage(mid, new HeapPageId(getId(), posOfPage),
                    Permissions.READ_ONLY);
            it = mypHeapPage.iterator();
        }
        public boolean hasNext() throws DbException, TransactionAbortedException, IOException, InterruptedException {
            if(it==null)
                return false;
            if(it.hasNext())
                return true;
            else if(posOfPage<numPages()-1)
            {
                posOfPage++;
                mypHeapPage = (HeapPage) Database.getBufferPool().getPage(mid, new HeapPageId(getId(), posOfPage),
                        Permissions.READ_ONLY);
                it=mypHeapPage.iterator();
                return it.hasNext();
            }
            return false;
        }

        public Tuple next()
                throws DbException, TransactionAbortedException, NoSuchElementException, IOException, InterruptedException {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return it.next();

        }

        public void rewind() throws DbException, TransactionAbortedException
        {
            try {
                it = mypHeapPage.iterator();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void close()
        {
             posOfPage=0;
             it=null;
        }
    }

}











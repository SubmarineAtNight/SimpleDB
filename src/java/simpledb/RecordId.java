package simpledb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

/**
 * A RecordId is a reference to a specific tuple on a specific page of a
 * specific table.
 */
public class RecordId implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a new RecordId referring to the specified PageId and tuple
     * number.
     * 
     * @param pid
     *            the pageid of the page on which the tuple resides
     * @param tupleno
     *            the tuple number within the page.
     */
    private  PageId myPid;
    private  int numTuple;
    public RecordId(PageId pid, int tupleno) {
        // some code goes here
        this.myPid=pid;
        this.numTuple=tupleno;
    }

    /**
     * @return the tuple number this RecordId references.
     */
    public int getTupleNumber() {
        // some code goes here
        return numTuple;
    }

    /**
     * @return the page id this RecordId references.
     */
    public PageId getPageId() {
        // some code goes here
        return myPid;
    }

    /**
     * Two RecordId objects are considered equal if they represent the same
     * tuple.
     * 
     * @return True if this and o represent the same tuple
     */
    @Override
    public boolean equals(Object o) {
        // some code goes here
        if(o.getClass()!=this.getClass())
            return false;
        RecordId newR=(RecordId) o;
        if(this.getTupleNumber()!=((RecordId) o).getTupleNumber())
            return false;
        if(o==this)
            return true;
        return (myPid.equals(((RecordId) o).myPid)) && (numTuple== ((RecordId) o).numTuple);

       // throw new UnsupportedOperationException("implement this");
    }

    /**
     * You should implement the hashCode() so that two equal RecordId instances
     * (with respect to equals()) have the same hashCode().
     * 
     * @return An int that is the same for equal RecordId objects.
     */
    @Override
    public int hashCode() {
        // some code goes here
        return Objects.hash(myPid, numTuple);
        //throw new UnsupportedOperationException("implement this");

    }

}
